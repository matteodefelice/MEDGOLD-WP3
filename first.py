import xarray as xr
import pandas as pd
import numpy as np
from  scipy import stats, signal
from matplotlib import pyplot as plt
from get_stations_ts import get_station
import cartopy.crs as ccrs
import cartopy.feature as cfeat

FILEPATH = '/opt/data/ERAIN/IP-tp-2008-2017.daysum.nc'
ds = xr.open_dataset(FILEPATH)
d = get_station(1)

# ds.tp.mean(dim = 'time').plot()
tp_clim = ds.groupby('time.dayofyear').mean(dim='time')
tp_anom = ds.groupby('time.dayofyear') - tp_clim
# ds_seasonal = ds.groupby('time.season').mean('time')



st_ds = tp_anom.sel(time = slice('2011-01-01', '2017-12-31')). drop('time_bnds').stack(x = ('latitude','longitude'))
def pt_corr(x):
    target = d.value.values # st_ds.tp[:, 100].values # np.arange(0, 3654)
    cf = np.corrcoef(x.tp.values[:,0], target)[0,1]
    return(xr.DataArray(cf))


cm = st_ds.groupby('x').apply(pt_corr)
cm = cm.unstack('x')
fig = plt.figure(figsize=(10, 4))

ax = fig.add_subplot(111, projection=ccrs.PlateCarree())
sst = ax.contourf(cm.x_level_1, cm.x_level_0, cm, 23, transform=ccrs.PlateCarree(), cmap='viridis')
ax.add_feature(cfeat.COASTLINE)
plt.colorbar(sst)

# ax = plt.axes(projection=ccrs.Orthographic(0, 35))
ax = plt.axes(projection=ccrs.Orthographic(0, 35))
cm.plot.contourf(ax=ax, transform=ccrs.PlateCarree(), levels = 23)
# add maximum
max_corr = cm.max()
x_max = cm.where(cm == cm.max(), drop = True).x_level_1
y_max = cm.where(cm == cm.max(), drop = True).x_level_0
plt.plot(x = x_max.values[0], y = y_max.values[0], color='green', marker='o')
plt.show() 

ax.set_global(); ax.coastlines(resolution='50m', color='black', linewidth=1)
ax.add_feature(cfeat.BORDERS); #ax.relim(); ax.autoscale_view()
ax.set_extent([-10, 5, 35, 45]); ax.gridlines()

