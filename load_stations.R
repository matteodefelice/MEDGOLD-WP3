library(lubridate)
library(tidyverse)
library(R.matlab)
# Growing degree days
gdd_mat = readMat('INDEXES_STATIONS/gdd_MONTHLY_SantaBarbara_1941-2017.mat')
gdd = tibble(year = floor(gdd_mat$anni.sta.com) %>% as.numeric(), 
             month = ((gdd_mat$anni.sta.com - floor(gdd_mat$anni.sta.com))*12 + 1) %>% as.numeric() %>% round(),
             gdd = gdd_mat$index.stat %>% as.numeric())
# Growing season avg temperature
gst_mat = readMat('INDEXES_STATIONS/GST_MONTHLY_SantaBarbara_1941-2017.mat')
gst = tibble(year = floor(gst_mat$anni.sta.com) %>% as.numeric(), 
             month = ((gst_mat$anni.sta.com - floor(gst_mat$anni.sta.com))*12 + 1) %>% as.numeric() %>% round(),
             gst = gst_mat$index.stat %>% as.numeric())
# Heat stress days
hw_mat = readMat('INDEXES_STATIONS/hw_ANNUAL_SantaBarbara_1941-2017.mat')
hw = tibble(year = hw_mat$anni.sta.com %>% as.numeric(), 
            hw = hw_mat$index.stat %>% as.numeric())
# SU
su_mat = readMat('INDEXES_STATIONS/su_ANNUAL_SantaBarbara_1941-2017.mat')
su = tibble(year = su_mat$anni.sta.com %>% as.numeric(), 
            su = su_mat$index.stat %>% as.numeric())
# TX10p
tx10p_mat = readMat('INDEXES_STATIONS/tx10p_ANNUAL_SantaBarbara_1941-2017.mat')
tx10p = tibble(year = tx10p_mat$anni.sta.com %>% as.numeric(), 
            tx10p = tx10p_mat$index.stat %>% as.numeric())
# WSDI
wsdi_mat = readMat('INDEXES_STATIONS/wsdi_ANNUAL_SantaBarbara_1941-2017.mat')
wsdi = tibble(year = wsdi_mat$anni.sta.com %>% as.numeric(), 
               wsdi = wsdi_mat$index.stat %>% as.numeric())
# WSDI_d
wsdi_d_mat = readMat('INDEXES_STATIONS/wsdi_d_ANNUAL_SantaBarbara_1941-2017.mat')
wsdi_d = tibble(year = wsdi_d_mat$anni.sta.com %>% as.numeric(), 
                wsdi_d = wsdi_d_mat$index.stat %>% as.numeric())
