import pandas as pd
import matplotlib.pyplot as plt
# %pylab
def get_station(id_station = 0, varname = 'Prec'): 
    # Load file
    STATION_FILE = 'station-data-stacked.csv'
    sdata = pd.read_csv(STATION_FILE, parse_dates=['date'])
    list_stations = sdata.station.unique()
    sel_data = sdata[(sdata.station == list_stations[id_station]) & (sdata.variable == varname) & (sdata.date < '2018-01-01')]
    return(sel_data)
