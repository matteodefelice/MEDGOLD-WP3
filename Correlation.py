import xarray as xr
import pandas as pd
import numpy as np
from  scipy import stats, signal
from matplotlib import pyplot as plt
from get_stations_ts import get_station
import cartopy.crs as ccrs
import cartopy.feature as cfeat

FILES = {"NCEP2": {'path':'/opt/data/NCEP2/prate.sfc.IP.2011-2017.nc', 'varname': 'prate'},
         "ERA5": {'path':'/opt/data/ERAIN/IP-tp-2008-2017.daysum.nc', 'varname': 'tp'}, 
         "EOBS": {'path':'/opt/data/EOBS/rr_0.25deg_reg_v17.0.IP.nc', 'varname': 'rr'},
         "CHIRPS": {'path':'/opt/data/CHIRP/chirps-v2.0.IP.2011-2017.nc', 'varname': 'precip'}}

METRIC = 'corr'

for DATASET_LABEL in FILES.keys():
    for ID_STATION in range(1, 9):

        print(DATASET_LABEL + ' ' + str(ID_STATION))
        FILEPATH = FILES[DATASET_LABEL]['path']
        ds = xr.open_dataset(FILEPATH)

        #tp_clim = ds.groupby('time.dayofyear').mean(dim='time')
        #tp_anom = ds.groupby('time.dayofyear') - tp_clim
        # ds_seasonal = ds.groupby('time.season').mean('time')

        d = get_station(ID_STATION)

        if DATASET_LABEL=='NCEP2':
            st_ds = ds.sel(time = slice(d.date.iloc[0], d.date.iloc[-1])).stack(x = ('lat','lon')) #NCEP2
        elif DATASET_LABEL=='CHIRPS':
                st_ds = ds.sel(time = slice(d.date.iloc[0], d.date.iloc[-1])).stack(x = ('latitude','longitude')) #CHIRPS
        elif DATASET_LABEL=='ERA5':
            ds.time.values = ds.time.values.astype('datetime64[D]') # truncate to day
            st_ds = ds.sel(time = slice(d.date.iloc[0], d.date.iloc[-1])).drop('time_bnds').stack(x = ('latitude','longitude')) # ERA5
        elif DATASET_LABEL=='EOBS':
            st_ds = ds.sel(time = slice(d.date.iloc[0], d.date.iloc[-1])).stack(x = ('latitude','longitude')) #EOBS

        def pt_corr(x):
            target = d.value.values 
            
            if (not np.isnan(target).any()):
                if DATASET_LABEL=='NCEP2':
                    cf = np.corrcoef(x.prate.values[:,0], target)[0,1] # NCEP2
                elif DATASET_LABEL=='CHIRPS':
                    cf = np.corrcoef(x.precip.values, target)[0,1] # CHIRPS
                elif DATASET_LABEL=='ERA5':
                    cf = np.corrcoef(x.tp.values[:,0], target)[0,1] # NCEP2
                elif DATASET_LABEL=='EOBS':
                    xm = np.ma.masked_array(x.rr.values, np.isnan(x.rr.values)) #EOBS
                    ym = np.ma.masked_array(target,np.isnan(target))
                    m  = ~(xm.mask|ym.mask)
                    cf = np.corrcoef(x.rr.values[m], target[m],)[0,1]
            else:
                if DATASET_LABEL=='ERA5':
                    xm = np.ma.masked_array(x.tp.values[:,0], np.isnan(x.tp.values[:,0])) #ERA5
                    ym = np.ma.masked_array(target,np.isnan(target))
                    m  = ~(xm.mask|ym.mask)
                    cf = np.corrcoef(x.tp.values[:,0][m], target[m],)[0,1]
                elif DATASET_LABEL=='CHIRPS':
                    xm = np.ma.masked_array(x.precip.values, np.isnan(x.precip.values)) #ERA5
                    ym = np.ma.masked_array(target,np.isnan(target))
                    m  = ~(xm.mask|ym.mask)
                    cf = np.corrcoef(x.precip.values[m], target[m],)[0,1]
                elif DATASET_LABEL=='NCEP2':
                    xm = np.ma.masked_array(x.prate.values[:,0], np.isnan(x.prate.values[:,0])) #ERA5
                    ym = np.ma.masked_array(target,np.isnan(target))
                    m  = ~(xm.mask|ym.mask)
                    cf = np.corrcoef(x.prate.values[:,0][m], target[m],)[0,1]
                elif DATASET_LABEL=='EOBS':
                    xm = np.ma.masked_array(x.rr.values, np.isnan(x.rr.values)) #EOBS
                    ym = np.ma.masked_array(target,np.isnan(target))
                    m  = ~(xm.mask|ym.mask)
                    cf = np.corrcoef(x.rr.values[m], target[m],)[0,1]

           
            if np.isnan(cf):
                return(xr.DataArray(-1))
            else:
                return(xr.DataArray(cf))

        def var_exp(x):
            target = d.value.values 
            cf = np.nanstd(x[FILES[DATASET_LABEL]['varname']].values) / np.nanstd(target)
             
            if np.isnan(cf):
                return(xr.DataArray(-1))
            else:
                return(xr.DataArray(cf))
        if METRIC == 'corr':
            cm = st_ds.groupby('x').apply(pt_corr)
        elif METRIC == 'varexp':
            cm = st_ds.groupby('x').apply(var_exp)
        cm = cm.unstack('x')

        fig = plt.figure(figsize=(10, 4))
        ax = plt.axes(projection=ccrs.Orthographic(0, 35))
        # cm.plot.contourf(ax=ax, transform=ccrs.PlateCarree(), levels = 23)
        cm.plot(ax=ax, transform=ccrs.PlateCarree())
        # add maximum
        max_corr = cm.max()
        if (DATASET_LABEL=='NCEP2') or (DATASET_LABEL=='ERA5'):
            x_max = cm.where(cm == cm.max(), drop = True).x_level_1
            y_max = cm.where(cm == cm.max(), drop = True).x_level_0
        else:
            x_max = cm.where(cm == cm.max(), drop = True).longitude
            y_max = cm.where(cm == cm.max(), drop = True).latitude
        ax.scatter(x = x_max.values[0], y = y_max.values[0], color='white', marker='+', transform=ccrs.PlateCarree())
        ax.annotate(str(round(float(max_corr.values), 2)), xy = (x_max.values[0], y_max.values[0]), xycoords = ccrs.PlateCarree()._as_mpl_transform(ax), color = 'white', fontsize = 'xx-large')
        plt.title('Prec - Correlation ' + DATASET_LABEL + ' and station ' + d.station.iloc[0])
        ax.set_global(); 
        ax.coastlines(resolution='50m', color='black', linewidth=1)
        ax.add_feature(cfeat.BORDERS);
        ax.add_feature(cfeat.RIVERS);#ax.relim(); ax.autoscale_view()
        ax.set_extent([-10, 5, 37, 44]); ax.gridlines()
        plt.savefig(str('prec_'+ METRIC+'_' + d.station.iloc[0] + '_' + DATASET_LABEL + '.png'), dpi = 300)
        plt.close()

